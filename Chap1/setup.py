from distutils.core import setup

setup(
    name = 'nester',
    version = '1.3.0',
    py_modules = ['nester'],
    author = 'md82',
    author_email = 'md82@tistory.com',
    url = "http://md82.tistory.com",
    description = "A simple printer of nested lists"
)